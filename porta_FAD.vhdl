-- Bloco FAD

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity FAD_ent is port (
	A: in std_logic;
	B: in std_logic;
	Ci: in std_logic;

	S: out std_logic;
	Co: out std_logic);
end FAD_ent;

architecture FAD_arc of FAD_ent is

signal S_temp_1 : std_logic:='0';
signal S_temp_2 : std_logic:='0';

signal Co_temp_1 : std_logic:='0';
signal Co_temp_2 : std_logic:='0';
signal Co_temp_3 : std_logic:='0';


begin

	HAD1: entity work.HAD_ent port map (A, B, S_temp_1, Co_temp_1);

	HAD2: entity work.HAD_ent port map (S_temp_1, Ci, S_temp_2, Co_temp_2);

	OR1: entity work.OR_ent port map (Co_temp_1, Co_temp_2, Co_temp_3);

	process(A, B)
	begin
		Co <= Co_temp_3;
		S <= S_temp_2;
	end process;

end FAD_arc;