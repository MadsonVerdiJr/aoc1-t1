-- PORTA HAD

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity HAD_ent is port (
	A: in std_logic;
	B: in std_logic;
	S: out std_logic;
	Co: out std_logic);
end HAD_ent;

architecture HAD_arc of HAD_ent is

signal Co_temp_1 : std_logic:='0';

begin


	AND1: entity work.AND_ent port map (A, B, Co_temp_1);

	process(A, B)
	begin
		S <= A xor B;
		Co <= Co_temp_1;
	end process;

end HAD_arc;