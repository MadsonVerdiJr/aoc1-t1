-- PORTA AND

library ieee;
use ieee.std_logic_1164.all;

entity AND_ent is port (
	A: in std_logic;
	B: in std_logic;
	S: out std_logic);
end AND_ent;

architecture AND_arc of AND_ent is
begin
	process(A, B)
	begin
		S <= A and B;
	end process;
end AND_arc;