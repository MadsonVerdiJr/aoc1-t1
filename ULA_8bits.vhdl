library ieee;
use ieee.std_logic_1164.all;

entity ULA_parametrizavel is
	generic (
		nbits: natural := 2);
	port(
		A: in std_logic_vector(nbits-1 downto 0);
		B:  in std_logic_vector(nbits-1 downto 0);
		Ci: in std_logic;
		Op: in std_logic_vector(1 downto 0);

		Co: out std_logic;
		S: out std_logic_vector(nbits-1 downto 0));
end ULA_parametrizavel;

architecture ULA_8 of ULA_parametrizavel is

		signal behv_Co: std_logic_vector(nbits-1 downto 0);

	begin
	ULA: FOR i IN 0 TO nbits-1 GENERATE

		bit_0: IF (i=0) GENERATE
			ULA_0: entity work.ULA_ent port map (
				A(i), 
				B(i), 
				Ci, 
				Op, 
				S(i), 
				behv_Co(i));
		END GENERATE bit_0;

		bit_x: IF (i>0) GENERATE
			ULA_X: entity work.ULA_ent port map (
				A(i), 
				B(i), 
				behv_Co(i - 1),  -- Carry out do anterior
				Op, 
				S(i), 
				behv_Co(i));
		END GENERATE bit_x;
	END GENERATE ULA;
	Co <= behv_Co(nbits -1);
end ULA_8;