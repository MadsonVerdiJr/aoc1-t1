-- Bloco ULA

library ieee;
use ieee.std_logic_1164.all;

entity ULA_ent is port (
	A: in std_logic;
	B: in std_logic;
	Cin: in std_logic;
	Op: in std_logic_vector (1 downto 0);
	
	S: out std_logic;
	Cout: out std_logic);
end ULA_ent;

architecture ULA_arc of ULA_ent is

signal S_and : std_logic:='0';
signal S_or : std_logic:='0';
signal S_not : std_logic:='0';
signal S_fad : std_logic:='0';

signal C_fad : std_logic:='0';

begin

	AND1: entity work.AND_ent port map (A, B, S_and);
	OR1: entity work.OR_ent port map (A, B, S_or);
	NOT1: entity work.NOT_ent port map (A, S_not);
	FAD1: entity work.FAD_ent port map (A, B, Cin, S_fad, C_fad);

	Cout <= C_fad;
	S <= S_and when (Op = "00") else
		S_or when (Op = "01") else
		S_not when (Op = "10") else
		S_fad;

end ULA_arc;