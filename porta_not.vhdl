-- PORTA NOT

library ieee;
use ieee.std_logic_1164.all;

entity NOT_ent is port (
	A: in std_logic;
	S: out std_logic);
end NOT_ent;

architecture NOT_arc of NOT_ent is
begin
	
	process(A)
	begin
		S <= not A;
	end process;

end NOT_arc;