library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity ULA_TB_ent is
end ULA_TB_ent;

architecture ULA_TB_arc of ULA_TB_ent is

	signal TA: std_logic;
	signal TB: std_logic;
	signal TCi: std_logic;
	signal TOp: std_logic_vector(1 downto 0);

	signal TCo: std_logic;
	signal TS: std_logic;

	signal counter: std_logic_vector(4 downto 0)  := "00000";

begin
	ULA: entity work.ULA_ent port map (TA, TB, TCi, TOp, TS, TCo);

	process
	begin

		if(TOp = "00") then
			assert ( TS = (TA and TB)) report "Erro na AND" severity warning; 
		elsif(TOp = "01") then
			assert ( TS = (TA or TB)) report "Erro na OR" severity warning;
		elsif(TOp = "10") then
			assert ( TS = not(TA)) report "Erro na NOT" severity warning;
		elsif(TOp = "11") then
			if(TCo = '1') then
				assert ( TS = (TA or TB)) report "FAD: overthread" severity note;
			else
				assert ( TS = (TA or TB)) report "Erro na FAD" severity warning;
			end if;
		end if;
		
		wait for 5 ns;

		counter <= counter + 1;



	end process;

	TA <= counter(0);
	TB <= counter(1);
	
	TCi <= counter(2);

	TOp <= counter(4 downto 3);
end ULA_TB_arc;