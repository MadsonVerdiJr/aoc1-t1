-- PORTA OR

library ieee;
use ieee.std_logic_1164.all;

entity OR_ent is port (
	A: in std_logic;
	B: in std_logic;
	S: out std_logic);
end OR_ent;

architecture OR_arc of OR_ent is
begin
	
	process(A, B)
	begin
		S <= A or B;
	end process;

end OR_arc;