library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity ULA8_TB_ent is
end ULA8_TB_ent;

architecture ULA8_TB of ULA8_TB_ent is

	signal TA: std_logic_vector(7 downto 0);
	signal TB: std_logic_vector(7 downto 0);
	signal TCi: std_logic;
	signal TOp: std_logic_vector(1 downto 0);

	signal TCo: std_logic := '0';
	signal TS: std_logic_vector(7 downto 0) := "00000000";

	signal counter: std_logic_vector(18 downto 0)  := "1100000000000000000";

begin
	ULA: entity work.ULA_parametrizavel generic map (
		nbits => 8)
		port map (
			A => TA, 
			B => TB,
			Ci => TCi, 
			Op => TOp,
			Co => TCo,
			S => TS
		);

	TA <= counter(7 downto 0);
	TB <= counter(15 downto 8); 
	TCi <= counter(16);
	TOp <= counter(18 downto 17);

	process
	begin

--		assert() report "string" 		wait for 20 ns;
		if(TOp = "00") then
			assert ( TS = (TA and TB)) report "Erro na AND" severity warning; 
		elsif(TOp = "01") then
			assert ( TS = (TA or TB)) report "Erro na OR" severity warning;
		elsif(TOp = "10") then
			assert ( TS = not(TA)) report "Erro na NOT" severity warning;
		elsif(TOp = "11") then
			if(TCo = '1') then
				assert ( TS = (TA + TB)) report "FAD: overthread" severity note;
			else
				assert ( TS = (TA + TB)) report "Erro na FAD" severity warning;
			end if;
		end if;

		wait for 5 ns;
		
		counter <= counter + 1;
	end process;
end ULA8_TB;